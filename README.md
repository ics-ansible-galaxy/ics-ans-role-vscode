# ics-ans-role-vscode

Ansible role to install vscode.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-vscode
```

## License

BSD 2-clause
